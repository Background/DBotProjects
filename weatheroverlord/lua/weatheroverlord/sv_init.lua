
-- Copyright (C) 2017-2018 DBot

-- Licensed under the Apache License, Version 2.0 (the "License");
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at

--     http://www.apache.org/licenses/LICENSE-2.0

-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.

local WOverlord = WOverlord
local DLib = DLib
local net = net
local hook = hook
local IsValid = IsValid

net.pool('weatheroverlord.replicateseed')

local function WOverlord_SeedChanges()
	net.Start('weatheroverlord.replicateseed')
	net.WriteUInt(WOverlord.SEED_VALID, 64)
	net.Broadcast()
end

net.receive('weatheroverlord.replicateseed', function(len, ply)
	if not IsValid(ply) then return end

	net.Start('weatheroverlord.replicateseed')
	net.WriteUInt(WOverlord.SEED_VALID, 64)
	net.Send(ply)
end)

hook.Add('WOverlord_SeedChanges', 'WeatherOverlord_ReplicateSeed', WOverlord_SeedChanges)
